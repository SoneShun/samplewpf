﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Collections.ObjectModel;
using System.Collections;

namespace SampleWPF
{
    public class Converter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Person _ = value as Person;
            string _name = _.Name;

            return _name;
        }

        //今回は実装なし
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<Person> _person = new List<Person>();

            try {
                using (var db = new DataClassesDataContext(ConfigurationManager.ConnectionStrings["SampleWPF.Properties.Settings.TIMECARDConnectionString"].ConnectionString))
                {
                    db.M_ACCOUNT.ToList().ForEach(x => {

                        _person.Add(new Person()
                        {
                            Name = x.USERNM,
                            ID = x.USERID,
                            //PassWord = x.PASS
                        });
                    });
                }
            }
            catch (Exception e){ }

            //this._person = new List<Person>() { _person };
            //public List<Person> _person { get; set; } 

            //MainViewModel viewModel = DataContext as MainViewModel;
            //this.DataContext = _person;
            //_person.ToString();
            
            listBox.DataContext = _person;
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var _date = DateTime.Now;
            _date.ToShortDateString();
            DateTime _YMB = DateUtility.Date.YMB(_date);
            DateTime _YME = DateUtility.Date.YME(_date);

            //クリックした部分のID取得
            var GetList = (sender as ListBox).SelectedItems;
            Person _GetList = GetList[0] as Person;
            string _GetID = _GetList.ID;

            List<WorkResult> _ = new List<WorkResult>();

            using (var db = new DataClassesDataContext(ConfigurationManager.ConnectionStrings["SampleWPF.Properties.Settings.TIMECARDConnectionString"].ConnectionString))
            {
                try
                {
                    var _result = db.R_TIMECARD.Where(x => x.USERID == _GetID).ToList();
                    List<R_TIMECARD> _result_detail = _result.Where(x => x.ARRIVE >= _YMB && x.ARRIVE <= _YME).ToList();
                    
                    //全ての月のデータ
                    _result.ToList().ForEach(x => {
                        _.Add(new WorkResult(){
                            Date = x.ARRIVE.Value.ToString("MM月dd日(ddd)"),
                            Arrive = x.ARRIVE.Value.ToShortTimeString(),
                            Leave = x.LEAVE.Value.ToShortTimeString()
                        });
                    });

                    //当月分のデータ
                    //_result_detail.ToList().ForEach(x => {
                    //    _.Add(new WorkResult() {
                    //        Date = x.ARRIVE.Value.ToString("MM月dd日(ddd)"),
                    //        Arrive = x.ARRIVE.Value.ToShortTimeString(),
                    //        Leave = x.LEAVE.Value.ToShortTimeString()
                    //    });
                    //});

                    this.DataContext = _;
                }
                catch (Exception ee) { }
            }
        }

        public class MainViewModel
        {
            public ObservableCollection<string> Collection { get; set; }
            public MainViewModel()
            {
                Collection = new ObservableCollection<string>();
            }

        }
    }

    public class Person
    {
        public string Name { get; set; }
        public string ID { get; set; }
        //public string PassWord { get; set; }
        //public List<WorkResult> WorkResult { get; set; }

        //コンストラクター
        public Person() { }
        public Person(string _Name, string _ID)
        {
            this.Name = _Name;
            this.ID = _ID;
        }
    }

    public class WorkResult
    {
        public string Date { get; set; }
        public string Arrive { get; set; }
        public string Leave { get; set; }

        //コンストラクター
        public WorkResult() { }
        public WorkResult(string _Date, string _Arrive, string _Leave)
        {
            this.Date = _Date;
            this.Arrive = _Arrive;
            this.Leave = _Leave;
        }
    }

    public class DateUtility
    {
        public static class Date
        {
            //1日の始まりと終わり
            static public DateTime MDT(DateTime _date, string _time)
            {
                DateTime _MDT = DateTime.Parse(_date.ToShortDateString() + _time);
                return _MDT;
            }

            //月の始まり
            static public DateTime YMB(DateTime _date)
            {
                DateTime _YMB = new DateTime(_date.Year, _date.Month, 1);
                return _YMB;
            }

            //月の終わり
            static public DateTime YME(DateTime _date)
            {
                DateTime _YME = new DateTime(_date.Year, _date.Month, 1).AddMonths(1).AddDays(-1);
                _YME = DateTime.Parse(_YME.ToShortDateString() + " 23:59:59");
                return _YME;
            }
        }
    }
 
}

